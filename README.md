University of Toronto (Winter 2014)
Daniel Almeida - danielalmeida.ti at gmail.com
Feb 04 2014

Simple demo of floodlight according to official MACTracker tutorial 
(http://docs.projectfloodlight.org/display/floodlightcontroller/How+to+Write+a+Module).

The most important piece of code added is in the following file: 
/src/main/java/net/floodlightcontroller/mactracker/MACTracker.java

There is also a header extractor implemented according to this tutorial: 
http://networkstatic.net/tutorial-to-build-a-floodlight-sdn-openflow-controller-module/.
It's in the file /src/main/java/net/floodlightcontroller/headerextract/HeaderExtract.java